Case-insensitive Files
======================

INTRODUCTION
------------

The Case Insensitive Files module allows users to use case insensitive URLs to
request static files, even when Apache `mod_rewite` is used to generate
SEO-friendly URLs.  While this should not be necessary when using the Apache
`mod_speling` module with `CheckSpelling on` and `CheckCaseOnly on`, the issue
is that `mod_rewrite` has priority over `mod_speling`.  So if one specifies a
file with the wrong case in /sites/all/files, it handled by Drupal, which does
a case-sensitive look-up and returns a 404.

With the Case Insensitive Files module, if a file is requested and it does not
exist, but exists with a different case, a redirect to that different file is
generated.

This module is similar to the Global Redirect module
(https://www.drupal.org/project/globalredirect), except that the Global Redirect
module does not handle unmanaged static files in the public file path.

The module only affects files in `file_public_path`, and it currently does not
correct the case of the file's parent path

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/case_insensitive_files

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/case_insensitive_files

REQUIREMENTS
------------

This module does not require any other modules. The Case Insensitive Files
Examples module can be run with or without this module enabled to demonstrate
its effect.

RECOMMENDED MODULES
-------------------

None.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/7/extend/installing-modules
for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.
When enabled the module intercepts requests for non-existent files in the
`file_public_path`, and attempts to redirect users to files that do exist
with a different case. Disabling the module halts the redirection.

TROUBLESHOOTING
---------------

If you are getting 404 errors when requesting files using the wrong case,
  check:

- Is the module is enabled?
- Is the path to the file in the correct case?
- Is the path to the file within the `file_public_path`?

FAQ
---

Q: Can I use this module with IIS or any other web server that is already
   case-insensitive?

A: Yes, although this module will not do anything on a case-insensitive web
   server.

Q: Are there plans to allow case-insensitive paths in addition to
   case-insensitive file names?

A: Yes, case-insensitive paths are on the todo list.

MAINTAINERS
-----------

Current maintainers:

* Michael Godin (mikegodin) -
 [https://www.drupal.org/u/mikegodin](https://www.drupal.org/u/mikegodin)

This project has been sponsored by:

* CommunicateHealth, Inc -
  [https://www.drupal.org/communicatehealth-inc](https://www.drupal.org/communicatehealth-inc)

CommunicateHealth was founded on one core principle: We think people deserve
clear and simple information about their health. We help our clients create
products and services that can be easily accessed, understood, and used by the
people who need them most. Whether it’s a smart phone app or a pictogram — we
will research, design, test, and tweak until we get it right. Visit
[https://communicatehealth.com](https://communicatehealth.com) for more
information.
