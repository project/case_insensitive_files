<?php

/**
 * @file
 * Generates redirects to files in "file_public_path" requested with wrong case.
 *
 * If a file is requested and it does not exist, but exists with a different
 * case in "file_public_path", a redirect to that different file is generated.
 *
 * The redirect only applies to files in "file_public_path".
 *
 * The module does not correct the case of the file's parent path.
 *
 * @todo Correct the case of the file's parent path.
 */

/**
 * Implements hook_help().
 */
function case_insensitive_files_help($path, $arg) {
  switch ($path) {
    case "admin/help#case_insensitive_files":
      $help = '<p>'
      . t('The Case Insensitive Files module allows users to use case insensitive URLs to request static files, even when Apache `mod_rewite` is used to generate SEO-friendly URLs.')
       . ' '
      . t('While this should not be necessary when using the Apache `mod_speling` module with `CheckSpelling on` and `CheckCaseOnly on`, the issue is that `mod_rewrite` has priority over `mod_speling`.')
       . ' '
      . t('So if one specifies a file with the wrong case in /sites/all/files, it handled by Drupal, which does a case-sensitive look-up and returns a 404.')
      . '</p>';
      $help .= '<p>'
      . t('With the Case Insensitive Files module, if a file is requested and it does not exist, but exists with a different case, a redirect to that different file is generated.')
      . '</p>';
      $help .= '<p>'
      . t('The module only affects files in `file_public_path`, and it currently does not correct the case of the file&quot;s parent path.')
      . '</p>';
      $link = 'https://www.drupal.org/project/case_insensitive_files';
      $options = array('attributes' => array(), 'html' => FALSE);
      $vars = array('text' => $link, 'path' => $link, 'options' => $options);
      $help .= '<p>'
      . t('For a full description of the module, visit the project page:')
      . theme('link', $vars) . '.</p>';
      return $help;
  }
}

/**
 * Implements hook_init().
 */
function case_insensitive_files_init() {

  $uri_target = drupal_substr(request_uri(), 1);

  // If the request is not in the public path, do nothing.
  $pub_path = variable_get('file_public_path', 'sites/default/files');
  $in_pub_path = _case_insensitive_files_strpos($uri_target, $pub_path) === 0;
  if (!$in_pub_path) {
    return;
  }

  // If the file seems to exist as specified, do nothing.
  if (file_exists($uri_target)) {
    return;
  }

  // If the request is in the private path, do nothing.
  $prv_path = variable_get('file_private_path', 'sites/default/files/private');
  $in_prv_path = _case_insensitive_files_strpos($uri_target, $prv_path) === 0;
  if ($in_prv_path) {
    return;
  }

  // List the files in the parent directory.
  $parent = dirname($uri_target);
  $file_array = glob($parent . '/*', GLOB_NOSORT);

  // If one of the files is a case-insensitive match, redirect to it.
  $uri_target_lc = drupal_strtolower($uri_target);
  foreach ($file_array as $file) {
    if (drupal_strtolower($file) === $uri_target_lc) {
      header("Location: /$file", TRUE, 302);
      exit();
    }
  }
}

/**
 * Finds the position of the first occurrence of a string in another string.
 *
 * Adapted from drupal 8's Unicode::strpos.
 *
 * @param string $haystack
 *   The string to search in.
 * @param string $needle
 *   The string to find in $haystack.
 * @param int $offset
 *   If specified, start the search at this number of characters from the
 *   beginning (default 0).
 *
 * @return int|false
 *   The position where $needle occurs in $haystack, always relative to the
 *   beginning (independent of $offset), or FALSE if not found. Note that
 *   a return value of 0 is not the same as FALSE.
 */
function _case_insensitive_files_strpos($haystack, $needle, $offset = 0) {
  global $multibyte;
  if ($multibyte == UNICODE_MULTIBYTE) {
    return mb_strpos($haystack, $needle, $offset);
  }
  else {

    // Remove Unicode continuation characters, to be compatible with
    // drupal_strlen() and drupal_substr().
    $haystack = preg_replace("", '', $haystack);
    $needle = preg_replace("", '', $needle);
    return strpos($haystack, $needle, $offset);
  }
}
