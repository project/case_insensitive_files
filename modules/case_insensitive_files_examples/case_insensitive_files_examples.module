<?php

/**
 * @file
 * Case-Insensitive Files Examples module file.
 *
 * Generates a simple page showing an image, first specified in the correct
 * case, and later specified with the wrong case.
 */

/**
 * Implements hook_help().
 */
function case_insensitive_files_examples_help($path, $arg) {
  switch ($path) {
    case "admin/help#case_insensitive_files_examples":
      $help = '<p>'
      . t('The Case Insensitive Files Examples module simply generates a simple page showing an image, first specified in the correct case, and later specified with the wrong case.')
      . '</p>';
      $help .= '<p>'
      . t('The Case Insensitive Files Examples module can be run with or  without the Case Insensitive Files module enabled to demonstrate its effect.')
      . '</p>';
      $link = 'https://www.drupal.org/project/case_insensitive_files';
      $options = array('attributes' => array(), 'html' => FALSE);
      $vars = array('text' => $link, 'path' => $link, 'options' => $options);
      $help .= '<p>'
      . t('For a full description of the module, visit the project page:')
      . theme('link', $vars) . '.</p>';
      return $help;
  }
}

/**
 * Implements hook_menu().
 */
function case_insensitive_files_examples_menu() {

  $items['case_insensitive_files/examples'] = array(
    'title' => 'Case insensitive files examples',
    'description' => 'Examples showing effect of Case-Insensitive Files module.'
    . ' Can be run with Case-Insensitive Files module disabled',
    'page callback' => '_case_insensitive_files_examples_example',
  );

  return $items;
}

/**
 * Page callback: Provides example page.
 *
 * Generates a simple page showing an image, first specified in
 * the correct case, and later specified with the wrong case.
 *
 * @return string
 *   The simple page content.
 */
function _case_insensitive_files_examples_example() {

  $path = '/' . variable_get('file_public_path', 'sites/default/files')
    . '/case_insensitive_files_examples';
  $src_ok = $path . '/case_insensitive_files_example.jpg';
  $src_bad = $path . '/Case_insensitivE_files_Example.jpg';
  $hdr_ok = t('Image at !src:', array('!src' => $src_ok));
  $hdr_bad = t('Image at !src (wrong case):', array('!src' => $src_bad));
  $alt_text_ok = t('Image saying "Case-Insensitive Files"');
  $alt_text_bad = $alt_text_ok . " " . t("but incorrect case used in URL");

  $output = <<<EOD
    <div class='case_insensitive_files_examples_example'>
      <h3>$hdr_ok</h3>
      <p>
        <img src='$src_ok' alt='$alt_text_ok'/>
      </p>
      <h3>$hdr_bad</h3>
      <p>
        <img src='$src_bad' alt='$alt_text_bad'/>
      </p>
    </div>
EOD;
  return $output;
}
